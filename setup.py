from setuptools import setup

setup(
    name= 'genutils',
    py_modules= ['genutils'],
    author= 'Peter Swain',
    author_email= 'peter.swain@ed.ac.uk',
    url= '',
    license='',
    description= 'General utility functions',
    python_requires= '>=3.6',
    include_package_data= True,
    install_requires= [
        'numpy'
        ]
)
